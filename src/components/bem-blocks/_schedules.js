function schedules() {
  let now = new Date();

  function dayOfWeek() {
    let today = now.getDay();
    let week = document.querySelectorAll('.header__schedule-day');
    if (!today) {
      week[6].classList.add('--active');
    } else {
      week[today - 1].classList.add('--active');
    }
  }
  dayOfWeek();

  function workTime() {
    let hours = now.getHours();
    let mins = now.getMinutes();
    let workText = document.querySelector('.header__work-now');
    if (hours >= 11) {
      if (hours <= 21 && mins <= 30) {
        workText.innerText = "сейчас открыто";
      } else {
        workText.style.color = "var(--color__red)";
        workText.innerText = "сейчас закрыто";
      }
    } else {
      workText.style.color = "var(--color__red)";
      workText.innerText = "сейчас закрыто";
    }
  }
  workTime();
}
schedules();